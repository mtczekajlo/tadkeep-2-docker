FROM rustlang/rust:nightly

RUN rustup target add thumbv6m-none-eabi

RUN rustup component add rustfmt clippy
